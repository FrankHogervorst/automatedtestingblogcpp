#include "post.hpp"


std::map<std::string, std::string> Post::dictionary()
{
    std::map<std::string, std::string> m;

    m["title"] = title;
    m["content"] = content;
    return m;
};
