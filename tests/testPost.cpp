#include "catch.hpp"
#include "post.hpp"


TEST_CASE("Create post", "[post]")
{
    std::string title = "TestTitle";
    std::string content = "TestContent";

    Post post(title, content); 

    REQUIRE(post.title == "TestTitle");
    REQUIRE(post.content == "TestContent");
}


TEST_CASE("Test dictionary", "[post]")
{
    std::string title = "TestTitle";
    std::string content = "TestContent";
    
    Post post(title, content); 
    auto dict = post.dictionary();


    REQUIRE(dict["title"] == "TestTitle");
    REQUIRE(dict["content"] == "TestContent");

}