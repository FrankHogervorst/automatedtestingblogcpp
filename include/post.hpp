#ifndef POST_H_
#define POST_H_

#include <iostream>
#include <map>
#include <string>

class Post
{
public:
    std::string title;
    std::string content;

    // Constructor
    Post(std::string title, std::string content): title(title), content(content){};
    
    
    // Constructor
	~Post(){}; 


    /*-------------------------------------------------------------------------
    Methods
    -------------------------------------------------------------------------*/
    std::map<std::string, std::string> dictionary();

};

#endif // POST_H